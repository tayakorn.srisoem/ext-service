package com.toffy.trainer.extjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExtjsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExtjsServiceApplication.class, args);
	}

}
