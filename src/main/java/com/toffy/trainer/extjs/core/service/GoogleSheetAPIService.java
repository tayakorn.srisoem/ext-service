package com.toffy.trainer.extjs.core.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.toffy.trainer.extjs.config.GoogleApiConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GoogleSheetAPIService {
	
	
	private Sheets credentialsGoogleSheet() throws GeneralSecurityException, IOException {
		log.info("CREDENTIALS_FOLDER: ", GoogleApiConfig.CREDENTIALS_FOLDER.getAbsolutePath());

		if (!GoogleApiConfig.CREDENTIALS_FOLDER.exists()) {
			GoogleApiConfig.CREDENTIALS_FOLDER.mkdirs();

			log.info("Created Folder: " + GoogleApiConfig.CREDENTIALS_FOLDER.getAbsolutePath());
			log.info("Copy file " + GoogleApiConfig.CLIENT_SECRET_FILE_NAME
					+ " into folder above.. and rerun this class!!");
		}

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Credential credential = GoogleApiConfig.getCredentials(HTTP_TRANSPORT);

		Sheets sheet = new Sheets.Builder(HTTP_TRANSPORT, GoogleApiConfig.JSON_FACTORY, credential).setApplicationName(GoogleApiConfig.APPLICATION_NAME).build();
		return sheet;
	}
	
	private ValueRange getValueRangeBySpreadsheetIdAndRange(String spreadsheetId, String range) throws IOException, GeneralSecurityException
	{
		return credentialsGoogleSheet().spreadsheets().values().get(spreadsheetId, range).execute();
	}
	
	public List<List<Object>> getGoogleSheetDataBySpreadsheetIdAndRange(String spreadsheetId, String range) throws IOException, GeneralSecurityException
	{
		return getValueRangeBySpreadsheetIdAndRange(spreadsheetId, range).getValues();
	}
	
	public Sheets updateGoogleSheet(String spreadsheetId,String sheetName,String range,List<List<Object>> values) throws IOException, GeneralSecurityException
	{
		Sheets service = credentialsGoogleSheet();
		ValueRange body = new ValueRange()
			.setValues(values);
		service.spreadsheets()
			.values()
			.update(spreadsheetId, sheetName+ range, body)
			.setValueInputOption("RAW")
			.execute();
		return service;
	}
}
