package com.toffy.trainer.extjs.emp.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.toffy.trainer.extjs.emp.model.PositionModel;
import com.toffy.trainer.extjs.emp.service.EMPService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/emp")
@Api(value = "Employee", description = "API Employee", tags = { "emp" })
public class EMPController {

    @Autowired
    EMPService employeeService;

    @ApiOperation(value = "Employee list information", response = String.class)
	@PostMapping(value = "/getEmployeeList")
	public String getEmployee(HttpServletRequest request) throws IOException, GeneralSecurityException{
		return employeeService.getEmployeeList();
	}

    @ApiOperation(value = "Employee position list information", response = PositionModel.class, responseContainer = "List")
	@PostMapping(value = "/getPositionList")
	public List<PositionModel> getPosition(HttpServletRequest request) throws IOException, GeneralSecurityException{
		return employeeService.getPositionList();
	}
    
}
